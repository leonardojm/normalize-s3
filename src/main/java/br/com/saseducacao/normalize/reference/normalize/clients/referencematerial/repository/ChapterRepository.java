package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.repository;

import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.domain.Chapter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChapterRepository extends JpaRepository<Chapter, Long> {
    Chapter findByBookIdAndNumber(Long bookId, Integer number);
}