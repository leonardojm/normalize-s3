package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
@Where(clause = "deleted = false")
public class Book extends BaseAuditEntity implements Serializable {
    private static final long serialVersionUID = 9042605014823514003L;

    @NotNull
    @NotBlank
    @Column(length = 45)
    private String name;

    private String code;

    @NotNull
    private Integer year;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "book",
            orphanRemoval = true
    )
    @JsonIgnore
    private List<Chapter> chapters = new ArrayList<>();

    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @JsonIgnore
    private Lecture lecture;

    public Book() {
    }

    public Book(Long id) {
        this.id = id;
    }

    public Book(String name, Integer year, Lecture lecture) {
        this.name = name;
        this.year = year;
        this.lecture = lecture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYear() { return year; }

    public void setYear(Integer year) { this.year = year; }

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<Chapter> listChapters) {
        this.chapters = listChapters;
    }

    public Lecture getLecture() {
        return lecture;
    }

    public void setLecture(Lecture lecture) {
        this.lecture = lecture;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
