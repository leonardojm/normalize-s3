package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table
@Where(clause = "deleted = false")
public class ChapterContent extends BaseAuditEntity implements Serializable {
    private static final long serialVersionUID = 1742939675976520695L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private Chapter chapter;

    private Boolean active;

    private Integer profile;

    private String contentPath;

    public Chapter getChapter() {
        return chapter;
    }

    public ChapterContent() {

    }

    public ChapterContent(Date createdAt , Date lastModifiedAt, Chapter chapter, Boolean active, Integer profile, String contentPath) {
        this.chapter = chapter;
        this.active = active;
        this.profile = profile;
        this.contentPath = contentPath;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getContentPath() {
        return contentPath;
    }

    public void setContentPath(String contentPath) {
        this.contentPath = contentPath;
    }

    public Integer getProfile() {
        return profile;
    }

    public void setProfile(Integer profile) {
        this.profile = profile;
    }
}
