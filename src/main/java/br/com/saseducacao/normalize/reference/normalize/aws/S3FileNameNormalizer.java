package br.com.saseducacao.normalize.reference.normalize.aws;

import java.text.Normalizer;

public class S3FileNameNormalizer {

    public String normalize(String fileName) {
        String normalized = Normalizer.normalize(fileName, Normalizer.Form.NFD);
        String replaced = normalized.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        return stripOrdinalSymbols(replaced);
    }

    private String stripOrdinalSymbols(String input) {
        return input.replace("º", "o").replace("ª", "a");
    }
}
