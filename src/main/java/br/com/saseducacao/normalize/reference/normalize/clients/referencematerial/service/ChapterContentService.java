package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.service;

import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.repository.ChapterContentRepository;
import org.springframework.stereotype.Service;

@Service
public class ChapterContentService {

    private final ChapterContentRepository chapterContentRepository;

    public ChapterContentService(ChapterContentRepository chapterContentRepository) {
        this.chapterContentRepository = chapterContentRepository;
    }
}