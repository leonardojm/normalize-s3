package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.repository;

import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.dto.MasterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class MasterRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public MasterDTO findData(Integer chapterNumber, Long bookId, Long lectureId, Long gradeId) {
        String sql =
                "select c.number chapternumber, b.name bookName, l.name LectureName, g.name gradeName " +
                        "from chapter c " +
                        "join book b on b.id = c.book_id " +
                        "join lecture l on l.id = b.lecture_id " +
                        "join grade g on g.id = l.grade_id " +
                        "where b.id = '" + bookId.toString() + "' " +
                        " and g.id = '" + gradeId.toString() + "' " +
                        " and l.id = '" + lectureId.toString() + "' " +
                        " and c.number = '" + chapterNumber.toString() + "' ";

        List<Map<String, Object>> query = jdbcTemplate.queryForList(sql);

        if (query.size() > 0) {
            Object chapterId = query.get(0).get("chapternumber");
            Object lectureName = query.get(0).get("lecturename");
            Object gradeName = query.get(0).get("gradename");
            Object bookName = query.get(0).get("bookname");

            MasterDTO masterDTO = new MasterDTO((Integer) chapterId, (String) lectureName, (String) gradeName, (String) bookName);

            return masterDTO;
        } else {
            System.out.println(" DONT EXIST !" );
        }

        return null;
    }
}
