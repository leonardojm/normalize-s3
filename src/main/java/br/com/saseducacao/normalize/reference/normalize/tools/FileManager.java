package br.com.saseducacao.normalize.reference.normalize.tools;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileManager {
    public static void writingFile(List<String> data, String filename) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(filename);
            for (String str : data) {
                writer.write(str + ";\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}