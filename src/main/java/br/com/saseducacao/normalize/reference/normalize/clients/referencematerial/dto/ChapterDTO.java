package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.dto;

import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.domain.ChapterContent;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ChapterDTO implements Serializable {
    private static final long serialVersionUID = 8020312160797961079L;

    private Long id;

    private Integer number;

    private String title;

    private String description;

    private Date releaseDate;

    private String answerKeyUrl;

    private List<ChapterContent> chapterContents;

    private Long bookId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getAnswerKeyUrl() {
        return answerKeyUrl;
    }

    public void setAnswerKeyUrl(String answerKeyUrl) {
        this.answerKeyUrl = answerKeyUrl;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public List<ChapterContent> getChapterContents() {
        return chapterContents;
    }

    public void setChapterContents(List<ChapterContent> chapterContents) {
        this.chapterContents = chapterContents;
    }
}
