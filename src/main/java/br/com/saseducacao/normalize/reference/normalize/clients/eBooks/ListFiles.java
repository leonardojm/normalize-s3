package br.com.saseducacao.normalize.reference.normalize.clients.eBooks;

import br.com.saseducacao.normalize.reference.normalize.aws.AmazonS3Manager;
import br.com.saseducacao.normalize.reference.normalize.aws.S3FileNameNormalizer;
import br.com.saseducacao.normalize.reference.normalize.clients.eBooks.filemanipulator.FileSlicer;
import br.com.saseducacao.normalize.reference.normalize.cuid.Cuid;
import br.com.saseducacao.normalize.reference.normalize.tools.FileManager;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.Tika;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Component
public class ListFiles {
    private final AmazonS3Manager amazonS3Manager;
    private S3FileNameNormalizer s3FileNameNormalizer;

    public ListFiles(AmazonS3Manager amazonS3Manager) {
        this.amazonS3Manager = amazonS3Manager;
    }

    public void listKeys(String folder) throws IOException {
        String clientRegion = amazonS3Manager.getS3Client().getRegionName();
        String bucketName = amazonS3Manager.getBucketName();

        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new ProfileCredentialsProvider())
                .withRegion(clientRegion)
                .build();

        System.out.println("Listing objects");
        List<String> responseQuery = new ArrayList<>();
        listAllFilesAndGenerateUpdate(new File(folder), s3Client, responseQuery);

        System.out.println("SIZE: " + responseQuery.size());
        System.out.println(responseQuery);

        FileManager.writingFile(responseQuery, "./output");
    }

    public List<String> listAllFilesAndGenerateUpdate(File folder, AmazonS3 amazonS3, List<String> responseQuery) throws IOException {
        System.out.println("In listAllfiles(File) method");

        File[] fileNames = folder.listFiles();
        for (File file : fileNames) {
            // if directory call the same method again
            if (file.isDirectory()) {
                listAllFilesAndGenerateUpdate(file, amazonS3, responseQuery);
            } else {
                System.out.println("Ler o Aquivo");
                System.out.println(file.getName());


                String cuidFileName = Cuid.createCuid();
                responseQuery.add("UPDATE e_book SET file_id = \'" + cuidFileName + "\' WHERE code = \'" + FilenameUtils.getBaseName(file.getName()) + "\'");

                FileSlicer fileSlicer = new FileSlicer();

                List<Path> paths = fileSlicer.splitFile(file.getPath(), 1);

                //Copy to Amazon
                try {
                    Tika tika = new Tika();
                    File cryptedFile = new File("./encrypt" + file.getName());
                    String skey = new String("mNP9J+*cBy78RH&u");

                    //byte[] iv = CryptoMagic.encrypt(skey, file, cryptedFile);

                    // Upload a file as a new object with ContentType and title specified.
//                    PutObjectRequest request = new PutObjectRequest(amazonS3Manager.getBucketName(), cuidFileName, file);
//                    ObjectMetadata metadata = new ObjectMetadata();
//                    metadata.setContentType(tika.detect(file));
//                    metadata.addUserMetadata("x-amz-meta-title", file.getName());
//                    metadata.setContentDisposition("inline; filename=" + file.getName());
//                    metadata.setCacheControl("no-cache,no-store,must-revalidate");
//                    request.setMetadata(metadata);
//                    amazonS3.putObject(request);
                    long tempoInicio = System.currentTimeMillis();

                    //CryptoMagic.decrypt(skey, cryptedFile, new File("./decrypt" + file.getName()));

                    System.out.println("Tempo Total: " + (System.currentTimeMillis() - tempoInicio));

                } catch (AmazonServiceException e) {
                    System.err.println(e.getErrorMessage());
                }
            }
        }
        return responseQuery;
    }
}