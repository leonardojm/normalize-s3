package br.com.saseducacao.normalize.reference.normalize;

import br.com.saseducacao.normalize.reference.normalize.aws.AmazonS3Manager;
import br.com.saseducacao.normalize.reference.normalize.clients.eBooks.ListFiles;
import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.repository.ChapterContentRepository;
import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.repository.ChapterRepository;
import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.repository.MasterRepository;
import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.service.ChapterContentService;
import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.service.ChapterService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

@SpringBootApplication
public class NormalizeReferenceMaterialApplication {

    private final AmazonS3Manager amazonS3Manager;

    private final ChapterService chapterService;

    private final ChapterRepository chapterRepository;

    private final ChapterContentService chapterContentService;

    private final ChapterContentRepository chapterContentRepository;

    private final MasterRepository masterRepository;

    public NormalizeReferenceMaterialApplication(AmazonS3Manager amazonS3Manager, ChapterService chapterService, ChapterRepository chapterRepository, ChapterContentService chapterContentService, ChapterContentRepository chapterContentRepository, MasterRepository masterRepository) {
        this.amazonS3Manager = amazonS3Manager;
        this.chapterService = chapterService;
        this.chapterRepository = chapterRepository;
        this.chapterContentService = chapterContentService;
        this.chapterContentRepository = chapterContentRepository;
        this.masterRepository = masterRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(NormalizeReferenceMaterialApplication.class, args);
    }

//    @Bean
//    public void initReferenceMaterialMethod() {
//        ListKeys L = new ListKeys(amazonS3Manager, chapterService, chapterRepository, chapterContentRepository, masterRepository);
//        L.listKeys();
//    }

    @Bean
    public void initDigitalBooksMethod() throws IOException {
        ListFiles listFiles = new ListFiles(amazonS3Manager);
        listFiles.listKeys("./files");
    }
}
