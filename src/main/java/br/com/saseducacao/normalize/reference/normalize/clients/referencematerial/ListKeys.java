package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial;

import br.com.saseducacao.normalize.reference.normalize.aws.AmazonS3Manager;
import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.dto.MasterDTO;
import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.repository.ChapterContentRepository;
import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.repository.ChapterRepository;
import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.repository.MasterRepository;
import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.service.ChapterService;
import br.com.saseducacao.normalize.reference.normalize.cuid.Cuid;
import br.com.saseducacao.normalize.reference.normalize.tools.FileManager;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ListKeys {

    private final AmazonS3Manager amazonS3Manager;

    private final ChapterService chapterService;

    private final ChapterRepository chapterRepository;

    private final ChapterContentRepository chapterContentRepository;

    private final MasterRepository masterRepository;

    public ListKeys(AmazonS3Manager amazonS3Manager, ChapterService chapterService, ChapterRepository chapterRepository, ChapterContentRepository chapterContentRepository, MasterRepository masterRepository) {
        this.amazonS3Manager = amazonS3Manager;
        this.chapterService = chapterService;
        this.chapterRepository = chapterRepository;
        this.chapterContentRepository = chapterContentRepository;
        this.masterRepository = masterRepository;
    }

    public void listKeys() {
        String clientRegion = amazonS3Manager.getS3Client().getRegionName();
        String bucketName = amazonS3Manager.getBucketName();

        try {
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new ProfileCredentialsProvider())
                    .withRegion(clientRegion)
                    .build();

            System.out.println("Listing objects");

            ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucketName);
            ListObjectsV2Result result;
            List<String> response = new ArrayList<>();

            do {
                result = s3Client.listObjectsV2(req);

                for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
                    String fullFilePath = objectSummary.getKey();

                    String keys[] = objectSummary.getKey().split("/");

                    String select = null;
                    String profile = keys[0];
                    Long bookId = Long.parseLong(keys[3]);

                    String originalFileName = keys[4];

                    Integer chapterNumber = Integer.parseInt(keys[4].split("_")[0]);
                    String chapterTitle = keys[4].split("_")[1];
                    Integer profileId = 2;
                    String cuidPath = Cuid.createCuid();

                    Long gradeId = Long.parseLong(keys[1]);
                    Long lectureId = Long.parseLong(keys[2]);


                    MasterDTO masterDTO = masterRepository.findData(chapterNumber, bookId, lectureId, gradeId);

                    System.out.println(masterDTO.getChapterNumber().toString() + "  " + masterDTO.getBookName()  + "  " + masterDTO.getLectureName()  + "  " + masterDTO.getGradeName()  + "  " + chapterTitle.replace(".zip", ""));

//                    if (profile.equalsIgnoreCase("Professor")) {
//                        profileId = 3;
//                    }

//                    Chapter chapter = chapterService.findByBookIdAndNumber(bookId, chapterNumber);


//                    if (chapter != null) {
//                        System.out.println(chapter.toString());
//                        chapter = chapterRepository.save(new Chapter(new Date(), new Date(), chapterNumber, chapterTitle.replace(".zip",""), null, new Book(bookId)));
//                    }
//
//                    ChapterContent chapterContent = chapterContentRepository.findByChapterIdAndProfile(chapter.getId(), profileId);
//                    chapterContentRepository.save(new ChapterContent(new Date(), new Date(), new Chapter(chapter.getId()), true, profileId, cuidPath));

                    try {
                        //s3Client.copyObject(objectSummary.getBucketName(), fullFilePath, objectSummary.getBucketName(), cuidPath + "/" + originalFileName);
                    } catch (AmazonServiceException e) {
                        System.err.println(e.getErrorMessage());
                        System.exit(1);
                    }
                }

                String token = result.getNextContinuationToken();
                System.out.println("Next Continuation Token: " + token);
                req.setContinuationToken(token);
            } while (result.isTruncated());
            FileManager.writingFile(response, "./output");
        } catch (AmazonServiceException e) {
            e.printStackTrace();
        } catch (SdkClientException e) {
            e.printStackTrace();
        }
    }
}