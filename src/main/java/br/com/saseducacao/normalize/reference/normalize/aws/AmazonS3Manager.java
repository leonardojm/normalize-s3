package br.com.saseducacao.normalize.reference.normalize.aws;

import ch.qos.logback.classic.Logger;
import com.amazonaws.services.s3.AmazonS3;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AmazonS3Manager {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(AmazonS3Manager.class);

    private final AmazonS3 s3Client;
    private final String bucketName;

    public AmazonS3Manager(AmazonS3 amazonS3Client,
                           @Value("${cloud.aws.credentials.bucketName}") String bucketName) {
        this.s3Client = amazonS3Client;
        this.bucketName = bucketName;
    }

    public AmazonS3 getS3Client() {
        return s3Client;
    }

    public String getBucketName() {
        return bucketName;
    }
}
