package br.com.saseducacao.normalize.reference.normalize.clients.eBooks.security;

import org.apache.shiro.crypto.CryptoException;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;

public class CryptoMagic {
    private static final String ALGORITHM = "Blowfish";
    private static final String TRANSFORMATION = "Blowfish";
    private static final String PADDING = "Blowfish/ECB/NoPadding";

    public static byte[] encrypt(String key, File inputFile, File outputFile)
            throws CryptoException {
        return doCrypto(Cipher.ENCRYPT_MODE, key, inputFile, outputFile);
    }

    public static void decrypt(String key, File inputFile, File outputFile)
            throws CryptoException {
        doCrypto(Cipher.DECRYPT_MODE, key, inputFile, outputFile);
    }

    private static byte[] doCrypto(int cipherMode, String key, File inputFile,
                                 File outputFile) throws CryptoException {
        try {
            Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            Cipher.getInstance(PADDING);

            //Generate IV
            byte [] iv = new byte[12];
            SecureRandom secureRandom = new SecureRandom();
            secureRandom.nextBytes(iv);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

            //Crypto
            cipher.init(cipherMode, secretKey);

            BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(inputFile));
            BufferedOutputStream outputStream = new BufferedOutputStream( new FileOutputStream(outputFile));

            int read = 0;
            int sizeFirstBlock = (int) (inputFile.length()*0.10);
            byte [] buffer = new byte[sizeFirstBlock];

            CipherInputStream cypherInputStream = new CipherInputStream(inputStream, cipher);

            int count = 0;

            while (read != -1) {
                if(count < 1){
                    read = cypherInputStream.read(buffer);
                }
                else{
                    read = inputStream.read(buffer);
                }

                if(read == -1) break;

                outputStream.write(buffer, 0, read);
                count ++;
            }

            inputStream.close();
            outputStream.close();
            cypherInputStream.close();

            //byte[] outputBytes = cipher.doFinal(inputBytes);

            return ivParameterSpec.getIV();

        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | IOException ex) {
            throw new CryptoException("Error encrypting/decrypting file", ex);
        }
    }
}
