package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table
@Where(clause = "deleted = false")
public class Chapter extends BaseAuditEntity implements Serializable {
    private static final long serialVersionUID = 4006267157273233051L;

    private Integer number;

    @Column(length = 100)
    private String title;

    @Column(length = 100)
    private String description;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private Date releaseDate;

    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @JsonIgnore
    private Book book;

    @OneToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "chapter")
    private AnswerKey answerKey;

    @Column
    private String contentPath;

    public Chapter() {
    }

    public Chapter(Long id) {
        this.id = id;
    }

    public Chapter(Date createdAt , Date lastModifiedAt, Integer number, String title, String description, Book book) {
        this.number = number;
        this.title = title;
        this.description = description;
        this.book = book;
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String descriptionChapter) {
        this.description = descriptionChapter;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public AnswerKey getAnswerKey() {
        return answerKey;
    }

    public void setAnswerKey(AnswerKey answerKey) {
        this.answerKey = answerKey;
    }

    public String getContentPath() {
        return contentPath;
    }

    public void setContentPath(String contentPath) {
        this.contentPath = contentPath;
    }

}