package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.repository;


import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.domain.ChapterContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChapterContentRepository extends JpaRepository<ChapterContent, Long> {

    ChapterContent findByChapterIdAndProfile(Long chapterId, Integer profile);
}