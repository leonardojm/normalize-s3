package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.dto;

import java.io.Serializable;

public class MasterDTO implements Serializable {
    private static final long serialVersionUID = 8020312160797961079L;

    private Integer chapterNumber;
    private String lectureName;
    private String gradeName;
    private String bookName;

    public MasterDTO(Integer chapterNumber, String lectureName, String gradeName, String bookName) {
        this.chapterNumber = chapterNumber;
        this.lectureName = lectureName;
        this.gradeName = gradeName;
        this.bookName = bookName;
    }

    public Integer getChapterNumber() {
        return chapterNumber;
    }

    public void setChapterNumber(Integer chapterNumber) {
        this.chapterNumber = chapterNumber;
    }

    public String getLectureName() {
        return lectureName;
    }

    public void setLectureName(String lectureName) {
        this.lectureName = lectureName;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    @Override
    public String toString() {
        return "MasterDTO{" +
                "chapterNumber=" + chapterNumber +
                ", lectureName='" + lectureName + '\'' +
                ", gradeName='" + gradeName + '\'' +
                ", bookName='" + bookName + '\'' +
                '}';
    }
}

