package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.domain;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
@Where(clause = "deleted = false")
public class Lecture extends BaseAuditEntity implements Serializable {
    private static final long serialVersionUID = -3069304635816203457L;

    @Column(length = 45)
    private String name;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "lecture"
    )
    private List<Book> books = new ArrayList<>();

    public Lecture() {
    }

    public Lecture(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
