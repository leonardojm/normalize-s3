package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.service;


import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.domain.Chapter;
import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.repository.ChapterRepository;
import org.springframework.stereotype.Service;

@Service
public class ChapterService {

    private final ChapterRepository chapterRepository;

    public ChapterService(ChapterRepository chapterRepository) {
        this.chapterRepository = chapterRepository;
    }

    public Chapter findByBookIdAndNumber(Long bookId, Integer number) {
        return chapterRepository.findByBookIdAndNumber(bookId, number);
    }
}