package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
@Where(clause = "deleted = false")
public class AnswerKey extends BaseAuditEntity implements Serializable {
    private static final long serialVersionUID = 1742939675976520695L;

    @OneToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private Chapter chapter;

    private Boolean active;

    @Column(length = 1024)
    private String name;

    @Column(length = 32)
    private String extension;

    private String fileId;

    private Integer size;

    private String checksum;

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
