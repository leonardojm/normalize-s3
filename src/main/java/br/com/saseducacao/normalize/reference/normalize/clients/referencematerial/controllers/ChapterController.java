package br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.controllers;

import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.domain.Chapter;
import br.com.saseducacao.normalize.reference.normalize.clients.referencematerial.service.ChapterService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;

@RestController
public class ChapterController {

    private final ChapterService chapterService;

    public ChapterController(ChapterService chapterService) {
        this.chapterService = chapterService;
    }

    @GetMapping("/books/{bookId}/chapters/{chapterId}")
    @ResponseStatus(OK)
    public Chapter getChapter(@PathVariable Long bookId,
                              @PathVariable Integer number) {
        return chapterService.findByBookIdAndNumber(bookId, number);
    }
}